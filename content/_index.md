---
title: 'ERA NFTd Proposal '
---

#### Global Macro

The ERA Working Group hosted by Xalgorithms Foundation would instantiate NFTds on an XRPL side chain to launch a new type of regulated marketplace. This market would be anchored to a globally-consistent, independently-verifiable method of calibrating comparative worth in a manner that is designed to be relevant to anyone in a given region, and among regions, at any given time, and through time. This is intended to be useful to central banks, multilateral banks, commercial banks, decentralized autonomous organizations, and private currency and quasi-currency operators. 


#### Global Meso

Xalgorithms would advance explicit NFT use cases at the center of global trade, commerce, finance and money by bridging the technical with the legal domain framed by the Model Law on Electronic Transferable Records (MLETR) This standard was adopted in 2017 by the United Nations Commission on International Trade Law (UNCITRAL), a consensus on trade and commerce legislation among the departments of justice of all members of the United Nations system. 

#### Global Micro

There is a long-standing adage in permaculture that “the farmer grows the soil; the soil grows the crops”. Our agent-based model, built in NetLogo, illustrates exactly this: numerous project investors across multiple jurisdictions spanning several different eco-zones, who undertake projects to enhance the quality and quantity of soil, and obtain ERA deposit receipts. These tradable instruments are of interest to both project investors and speculators. Modeling is useful to envision the system we have in mind, but we are planning for this project to have prompt tangible benefits for real people while incrementally securing long term assurance of primary commodities and ecosystem integrity. Also, although soil-enhancement projects are the simplest to illustrate, our participating ecological data scientists are preparing for a global ERA NFTd marketplace that will draw upon dozens of indicators across 108 ecosystem functional groups, spanning terrestrial, marine and wetland types. Our 'crawl-walk-run' deployment trajectory as outlined in the "ERA NFTd Schedule" document shows how we intend to scale this collaborative undertaking.